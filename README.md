ownCloud Data Container
=======================

Very simple Dockerfile to create a data container to hold ownCloud data and configuration file(s).

It creates 2 volumes:

* /var/www/html/data
* /var/www/html/config
