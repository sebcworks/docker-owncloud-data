# Simple data container for ownCloud

FROM debian:jessie
MAINTAINER Sebastien Collin <sebastien.collin@sebcworks.fr>

# Make owncloud directories

RUN mkdir -p /var/www/html/data && \
    mkdir /var/www/html/config && \
    chown www-data:www-data /var/www/html/data && \
    chown www-data:nogroup /var/www/html/config && \
    chmod 770 /var/www/html/data && \
    chmod 750 /var/www/html/config

VOLUME /var/www/html/data
VOLUME /var/www/html/config

CMD ["echo", "ownCloud-Data"]
